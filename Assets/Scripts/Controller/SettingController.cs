﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Linq;

public class SettingController : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] Transform parentList;
    [SerializeField] GameObject prefabList;
    [SerializeField] GameObject lastObject;

    private void Awake()
    {
        Load();
    }

    private void Load()
    {
        if (parentList.childCount > 1)
        {
            foreach (Transform t in parentList)
            {
                if (t.GetSiblingIndex() < parentList.childCount - 1)
                    Destroy(t.gameObject);
            }
        }

        // Get All members name
        List<Member> allMembers = new List<Member>();
        JSONNode jsonStr = JSON.Parse(PlayerPrefs.GetString("data"));

        for (int i = 0; i < jsonStr.Count; i++)
        {
            string name = jsonStr[i]["name"].Value;
            //Gender gender = (Gender)System.Enum.Parse(typeof(Gender), jsonStr[i]["gender"].Value);

            allMembers.Add(new Member(name));
        }

        // Sort by name
        allMembers = allMembers.OrderBy(member => member.Name).ToList();

        for (int i = 0; i < allMembers.Count; i++)
        {
            ListName list = Instantiate(prefabList, parentList).GetComponent<ListName>();
            list.SetName(allMembers[i].Name);
            //list.SetGender(allMembers[i].Gender);
        }

        lastObject.transform.SetAsLastSibling();
    }

    public void Add()
    {
        Instantiate(prefabList, parentList);
        lastObject.transform.SetAsLastSibling();
    }

    public void Save()
    {
        string jsonStr = "[";

        foreach (Transform t in parentList)
        {
            if (t.GetComponent<ListName>() == null)
                continue;

            ListName item = t.GetComponent<ListName>();
            if (string.IsNullOrEmpty(item.GetName()))
                continue;

            jsonStr += "{" +
                " \"name\" : \" " + item.GetName() + "\" " +
                "}";

            // If last child
            if (t.GetSiblingIndex() < parentList.childCount - 2)
                jsonStr += ",";
        }

        jsonStr += "]";

        Debug.Log(jsonStr);

        PlayerPrefs.SetString("data", jsonStr);
    }

    public void ResetData()
    {
        PlayerPrefs.SetString("data", Resources.Load<TextAsset>("members").text);
        Load();
    }
}