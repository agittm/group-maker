﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class MainController : MonoBehaviour
{
    public enum RandomMode { SesuaiKelompok = 0, SesuaiAggota = 1 }

    private static int _groupCount = 0;
    private static List<string> _members = new List<string>();

    [Header("Component")]
    [SerializeField] GameObject[] panelMode;
    [SerializeField] GameObject[] buttonMode;
    [SerializeField] Transform parentList;
    [SerializeField] GameObject prefabList;
    [SerializeField] InputField inputGroupCount;
    [SerializeField] InputField inputMemberCount;

    /*[Header("Setting")]
    [SerializeField] Toggle bagiRataCewekCowok;
    [SerializeField] Toggle dipisahCewekCowok;*/

    private RandomMode mode = RandomMode.SesuaiKelompok;

    private void Awake()
    {
        // First time init
        if (!PlayerPrefs.HasKey("data"))
        {
            PlayerPrefs.SetString("data", Resources.Load<TextAsset>("members").text);
        }
    }

    private void Start()
    {
        if (_members.Count > 0)
        {
            Generate(_groupCount, _members);
        }

        SetMode(mode);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Delete))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    public void SetMode(string sMode)
    {
        if(sMode == "anggota")
        {
            SetMode(RandomMode.SesuaiAggota);
        }
        else if (sMode == "kelompok")
        {
            SetMode(RandomMode.SesuaiKelompok);
        }
    }

    public void SetMode(RandomMode randomMode)
    {
        mode = randomMode;

        for (int i = 0; i < panelMode.Length; i++)
        {
            panelMode[i].SetActive(i == (int)mode);
            buttonMode[i].SetActive(i == (int)mode);
        }
    }

    public void Generate()
    {
        // Get All members name
        List<Member> allMembers = new List<Member>();
        JSONNode jsonStr = JSON.Parse(PlayerPrefs.GetString("data"));

        for (int i = 0; i < jsonStr.Count; i++)
        {
            string name = jsonStr[i]["name"].Value;
            //Gender gender = (Gender)System.Enum.Parse(typeof(Gender), jsonStr[i]["gender"].Value);

            allMembers.Add(new Member(name));
        }

        // get group count
        int groupCount = 0;

        if (mode == RandomMode.SesuaiKelompok)
        {
            groupCount = int.Parse(inputGroupCount.text);
        }
        else
        {
            groupCount = Mathf.FloorToInt(allMembers.Count / int.Parse(inputMemberCount.text));
        }

        if (groupCount <= 1 || groupCount >= allMembers.Count)
            return;

        // Destroy previous generated list
        DestroyPrevList();

        // determine grid size
        DetermineGridSize(allMembers.Count, groupCount);

        // Create group list item
        List<ListItem> listGroup = CreateListItem(groupCount);

        // Randomize
        allMembers = allMembers.OrderBy(x => Random.value).ToList();

        if (mode == RandomMode.SesuaiKelompok)
        {
            if (string.IsNullOrEmpty(inputGroupCount.text))
                return;

            /*if (bagiRataCewekCowok.isOn)
            {
                // Sort dari cewe dulu
                allMembers = allMembers.OrderBy(x => x.Gender).Reverse().ToList();
            }*/

            _members.Clear();
            _groupCount = groupCount;

            // Add member to each list item
            for (int i = 0; i < allMembers.Count; i++)
            {
                listGroup[i % groupCount].AddMember(allMembers[i].Name);
                _members.Add(allMembers[i].Name);
            }
        }
        else if (mode == RandomMode.SesuaiAggota)
        {
            if (string.IsNullOrEmpty(inputMemberCount.text))
                return;
            
            int maxMember = allMembers.Count / groupCount + (allMembers.Count % groupCount);
            int groupNo = 0;
            int memberIndex = 0;

            foreach (Member member in allMembers)
            {
                listGroup[groupNo].AddMember(allMembers[memberIndex].Name);
                memberIndex++;

                if (listGroup[groupNo].GetMemberCount() >= maxMember)
                    groupNo++;
            }
        }
    }

    public void Generate(int groupCount, List<string> members)
    {
        // Destroy previous generated list
        DestroyPrevList();

        // determine grid size
        DetermineGridSize(members.Count, groupCount);

        // Create group list item
        List<ListItem> listGroup = CreateListItem(groupCount);

        // Add member to each list item
        for (int i = 0; i < members.Count; i++)
        {
            listGroup[i % groupCount].AddMember(members[i]);
        }
    }

    private List<ListItem> CreateListItem(int groupCount)
    {
        List<ListItem> listGroup = new List<ListItem>();

        for (int i = 0; i < groupCount; i++)
        {
            ListItem item = Instantiate(prefabList, parentList).GetComponent<ListItem>();
            item.SetGroupName("Kelompok " + (i + 1));
            listGroup.Add(item);
        }

        return listGroup;
    }

    private void DestroyPrevList()
    {
        if (parentList.childCount > 0)
        {
            foreach (Transform t in parentList)
            {
                Destroy(t.gameObject);
            }
        }
    }

    private void DetermineGridSize(int memberCount, int groupCount)
    {
        if (memberCount / groupCount <= 7)
        {
            parentList.GetComponent<GridLayoutGroup>().constraintCount = 2;
            parentList.GetComponent<GridLayoutGroup>().cellSize = new Vector2(parentList.GetComponent<GridLayoutGroup>().cellSize.x, 900);
        }
        else
        {
            parentList.GetComponent<GridLayoutGroup>().constraintCount = 1;
            parentList.GetComponent<GridLayoutGroup>().cellSize = new Vector2(parentList.GetComponent<GridLayoutGroup>().cellSize.x, 1600);
        }
    }
}