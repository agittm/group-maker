﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController
{
    public static void LoadScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene(sceneName, mode);
    }

    public static void ReloadScene()
    {
        SceneManager.LoadScene(GetCurrentScene().name);
    }

    public static Scene GetCurrentScene()
    {
        return SceneManager.GetActiveScene();
    }
}