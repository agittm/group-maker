﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Member
{
    public string Name;
    public Gender Gender;

    public Member(string name)
    {
        Name = name;
    }

    public Member(string name, Gender gender)
    {
        Name = name;
        Gender = gender;
    }
}

public enum Gender { L=0, P=1 };
