﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItem : MonoBehaviour
{
    [SerializeField] Text groupName;
    [SerializeField] InputField groupMembers;

    private int memberCount = 0;

    public void SetGroupName(string name)
    {
        groupName.text = name;
    }

    public void AddMember(string name)
    {
        groupMembers.text += name + "\n";
        memberCount++;
    }

    public int GetMemberCount()
    {
        return memberCount;
    }
}
