﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListName : MonoBehaviour
{
    [SerializeField] InputField inputName;
    //[SerializeField] Dropdown gender;

    public void SetName(string name)
    {
        inputName.text = name;
    }

    public string GetName()
    {
        return inputName.text;
    }

    /*public void SetGender(Gender g)
    {
        gender.value = (int)g;
    }

    public Gender GetGender()
    {
        return (Gender)System.Enum.Parse(typeof(Gender), gender.options[gender.value].text);
    }*/

    public void Remove()
    {
        Destroy(gameObject);
    }
}
