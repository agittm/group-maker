﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSceneButton : MonoBehaviour {

    [SerializeField] SceneField targetScene;

    public void LoadScene()
    {
        SceneController.LoadScene(targetScene.SceneName);
    }
}
